import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { UserService } from '../_services/user.service';
import { AuthenticationService } from '../_services/authentication.service';
//import { StorageService } from '../_services/storage.service';

import { ERROR_COMPONENT_TYPE } from '@angular/compiler';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  constructor(
      private router: Router,
      private authenticationService: AuthenticationService,
      //private storageService: StorageService
    ) { }


  ngOnInit() {
      // reset login status
      this.authenticationService.logout();
   //   this.isAdmin = localStorage.getItem('currentRole') === 'Authorized';
   //   console.log(localStorage.getItem('currentRole'));
  }

/*
  isAdmin: boolean = localStorage.getItem('currentRole') === 'Authorized';
  changeValue(valid: boolean) {
    this.isAdmin = valid;
       }
*/
  login() {
      this.loading = true;
      this.authenticationService.login(this.model.username, this.model.password)
      .catch(error => {
        this.error = 'Username or password is incorrect';
        this.loading = false;
        return Observable.throw(error);
        //return Observable.throw(error.status);
      })
      .subscribe(
            result => {
              if (result === true) {
                  // login successful
                  // this.storageService.setItem('currentRole','Auth');
                  this.router.navigate(['/']);
              } else {
                  // login failed
                  this.error = 'Username or password is incorrect';
                  this.loading = false;
              }
          }
        )

        ;

  }
}
