import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {StorageService} from './storage.service';
import { API_HOST, API_OAUTH, client_id, client_secret } from '../app.consts';

@Injectable()
export class AuthenticationService {
    public token: string;
    //private storageService: StorageService;

    constructor(private http: Http,  private storageService: StorageService) {
        // set token if saved in local storage

        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;

    }

    login(username: string, password: string): Observable<boolean> {
        const body = {username: username,
                      password: password,
                      client_secret: `${client_secret}`,
                      client_id: `${client_id}`,
                      grant_type: 'password'
                    };

       return this.http.post(`${API_HOST}/${API_OAUTH}`, body)
            .map(
                (response: Response) => {

           // login successful if there's a jwt token in the response
                const token = response.text();
                const line = response.text();
                console.log(response.status);

                let body:string = JSON.parse(response['_body']);

                if (line) {
                    const access_token = body['access_token'];
                    const refresh_token = body['refresh_token'];
                    const expires_in = body['expires_in'];

                        this.saveTokenInfo(token);

                        this.storageService.setItem('currentUser',JSON.stringify({
                          username: username,
                          access_token: access_token,
                          refresh_token: refresh_token,
                          expires_in: expires_in
                        }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            })
            .catch(error => {
                return Observable.throw(error.status);
             });
  // );
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.storageService.removeItem('currentUser');
        this.storageService.removeItem('Token');
        this.storageService.removeItem('currentRole');

    }


    saveTokenInfo(token): void {
         this.storageService.setItem('currentRole','Auth');
         this.storageService.setItem('Token',token);
    }

}
