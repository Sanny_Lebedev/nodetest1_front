import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';
import { Phones } from '../_models/phones';
import {StorageService} from './storage.service';
import {HttpService} from './http.service';
import { API_HOST, API_GETPHONE, API_PHONE } from '../app.consts';
import { Body } from '@angular/http/src/body';

@Injectable()
export class PhonesService {

  private curToken;
  private headers = new Headers();
  private phonesUrl = `${API_HOST}/${API_GETPHONE}`;
  private phoneapiUrl = `${API_HOST}/${API_PHONE}`;
  //private phonesUrl = 'http://localhost:3100/api/getphones';  // Get all phone numbers
  //private phoneapiUrl = 'http://localhost:3100/api/phone';
  private errorPhoneNum = /([^\-\s\(\)\+0-9]|\s{2,})/;


  constructor (private http: HttpService,) {}

  isValidPhone(myPhone:string) {
    return !this.errorPhoneNum.test(myPhone);

 }

  getPhones(): Observable<number> {

    return this.http.post(this.phonesUrl, null ,  {headers: this.headers})
      .map((data: Response) => data.json().numbers)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  delPhone(num:string):Observable<boolean> {
    const options = new RequestOptions({
      headers: this.headers,
      body: {
        number : num
      }
    });

    return this.http.delete(this.phoneapiUrl, options)
    .map((data: Response) => data.json())
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }


  addPhone(num:string):Observable<boolean> {
    const options = new RequestOptions({
      headers: this.headers,
      body: {
        number : num
      }
    });

    return this.http.put(this.phoneapiUrl, null ,  options)
    .map((data: Response) => data.json())
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
