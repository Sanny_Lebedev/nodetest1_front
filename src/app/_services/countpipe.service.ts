import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
    name: 'count'
})
@Injectable()
export class CountPipe implements PipeTransform {
    transform(items: any[], cb): any[] {
        if (!items) {
          cb(0);
          return [];
        }
        cb(items.length);
        return items;
    }
}
