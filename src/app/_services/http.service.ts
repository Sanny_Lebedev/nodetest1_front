import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { API_HOST, API_OAUTH, client_id, client_secret } from '../app.consts';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ObservableInput } from 'rxjs/Observable';

interface RequestParams {
  url: string | Request;
  options?: RequestOptionsArgs;
}

@Injectable()
export class HttpService extends Http {

  constructor(backend: XHRBackend, options: RequestOptions) {
    const accessToken =  JSON.parse(localStorage.getItem('Token')).access_token;
    if (accessToken !== null) {
      options.headers.set('Authorization', `Bearer ${accessToken}`);
    }
    super(backend, options);
  }
  get accessToken(): string {
    return JSON.parse(localStorage.getItem('Token')).access_token;
  }

  set accessToken(token: string) {
    localStorage.setItem('Token', token);
  }

  /*
  set Token(token: string) {
    localStorage.setItem('Token', token);

  }
 */


  get refreshToken(): string {
    return JSON.parse(localStorage.getItem('Token')).refresh_token;
  }

  set refreshToken(token: string) {
    localStorage.setItem('Token', token);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    const params = this.buildRequestParams(url, this.accessToken, options);
    return super.request(params.url, params.options).catch((err): ObservableInput<Response> => {
      console.log(err);
      if (err.status === 401) {
        return this.refreshAccessToken().flatMap((newToken) => {

          localStorage.setItem('Token', newToken.text());

          const newParams = this.buildRequestParams(url, this.accessToken, options);
          return super.request(newParams.url, newParams.options);
        }).catch((refreshErr): ObservableInput<any> => {

          return Observable.throw(refreshErr);
        });
      } else {

        return Observable.throw(err);
      }
    });
  }
  buildRequestParams(url: string | Request, token: string, options?: RequestOptionsArgs): RequestParams {
    if (typeof url === 'string') {
      if (!options) {
        options = { headers: new Headers() };
      }
      if (!options.hasOwnProperty('headers')) {
        options.headers = new Headers();
      }
      options.headers.set('Authorization', `Bearer ${token}`);
    } else {
      url.headers.set('Authorization', `Bearer ${token}`);
    }
    return { url: url, options: options };
  }
  refreshAccessToken(): Observable<Response> {

    const params = this.buildRequestParams('', this.refreshToken, { method: 'POST',
    body: {grant_type:'refresh_token', client_id: `${client_id}`, refresh_token: this.refreshToken, client_secret: `${client_secret}`  } });
    return super.request(
      `${API_HOST}/${API_OAUTH}`,
      params.options,
    );
  }
}
