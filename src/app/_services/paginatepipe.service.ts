import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginate'
})
@Injectable()
export class PaginatePipe implements PipeTransform {
  transform(items: any[], currentPage: number, perPage: number): any[] {
    if (!items) {
      return [];
    }
    if (currentPage === undefined) {
      currentPage = 1;
    }
    if (perPage === undefined) {
      perPage = 10;
    }
    const start = (currentPage - 1) * perPage;
    return items.slice(start, start + perPage);
  }
}
