import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Phones } from '../_models/phones';
import { Alerts } from '../_models/alerts';
import { PhonesService } from '../_services/phones.service';
import { ERROR_COMPONENT_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [PhonesService]
})
export class DashboardComponent implements OnInit {
  phones: Phones[];
  alerts: Alerts[] = [];
  private alertAccesserror: {
                        type: 'danger',
                        text: 'Access error'
  }

  private alertWrongnumber: {
    type: 'danger',
    text: 'Wrong phone number'
  }
  private errorPhoneNum = /([^\-\s\(\)\+0-9]|\s{2,})/;
  error = '';
  success = '';
  totalItems: number;
  phonesKeys: string[];
  phoneValue: string = '';
  currentPage: number;
  itemsPerPage: number;
  paginationMaxSize: number;
  smallnumPages: number;
  paginationCallback: (number) => void;

  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }


  constructor(private phoneService: PhonesService) {

  }

  isValidPhone(myPhone){
    return !this.errorPhoneNum.test(myPhone);
 }


  getPhones(): void {

    this.phoneService.getPhones().subscribe((phones: any) => {
      this.phones = phones;
      this.phonesKeys = Object.keys(phones);
      this.totalItems = phones.length;
      console.log(phones.id, this.phonesKeys, phones.length);
    },
    err => {
      console.log('ОЙ' + err);
      this.alerts.push(this.alertAccesserror);
     });
  }

  delPhones(num): void {
     this.phoneService.delPhone(num).subscribe((result: any) => {

       this.alerts.push({
        type: result.alert,
        text: result.message});

        if (result.success) { this.getPhones();}

    },
    err => { this.alerts.push(this.alertAccesserror);
              });
  }


  ngOnInit() {

    this.alertAccesserror =  {
      type: 'danger',
      text: 'Access error'
    }


    // Initial setting of the paginator
    this.currentPage = 1;
    this.totalItems = 1;
    this.itemsPerPage = 10;
    this.paginationMaxSize = 10;

    // Sending a request to receive a list of phone numbers
    this.getPhones();
    this.paginationCallback = (numberOfItems: number) => {
    this.totalItems = numberOfItems;
    };
  }

  public pageChanged(event: any): void {
    this.setPage(event.page);
  }

  public delnum (num: string){
    // Zero data for the paginator
    this.currentPage = 1;
    this.totalItems = 1;

    // Sending request to delete the phone
    this.delPhones(num);
  }


  public addphone(num: string){
    if (!this.isValidPhone(num)) {
      // Number format is incorrect
      this.alerts.push({
        type: 'danger',
        text: 'Wrong phone number'
      });
    } else {
      // The implementation of adding numbers
      this.phoneService.addPhone(num)
        .subscribe((result: any) => {
            this.alerts.push({
                  type: result.alert,
                  text: result.message});

            if (result.success) {
              this.getPhones();
              this.phoneValue = null;
            }
     },
     err => { this.alerts.push({
                         type: 'danger',
                         text: 'Error' + err
          });
              console.log(err);
     });



    }
  }
}
