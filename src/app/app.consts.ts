import { isDevMode } from '@angular/core';
import { environment } from '../environments/environment';

export const API_HOST = !environment.production ? 'http://localhost:3100/api' : 'http://nodetest1.agency911.org/api';
export const API_OAUTH = 'oauth';
export const client_id = '1';
export const client_secret = '1';
export const API_GETPHONE = 'getphones';
export const API_PHONE = 'phone';
