import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { LoggedInGuard } from './_guards/logged-in.guard';
// import { UserService } from './_services/user.service';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [LoggedInGuard]},
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  // providers: [UserService, LoggedInGuard]
})
export class AppRoutingModule { }
