import { BrowserModule } from '@angular/platform-browser';
import { HttpModule} from '@angular/http';

import { NgModule } from '@angular/core';

import { SortableModule } from 'ngx-bootstrap/sortable';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CollapseModule, BsDropdownModule, AlertModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { HttpService } from './_services/http.service';
import { PaginatePipe } from './_services/paginatepipe.service';
import { CountPipe } from './_services/countpipe.service';


import { LoggedInGuard } from './_guards/logged-in.guard';
import { UserService } from './_services/user.service';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

import { AuthenticationService} from './_services/authentication.service';
import { StorageService} from './_services/storage.service';


@NgModule({
  declarations: [
    AppComponent,
    PaginatePipe,
    CountPipe,
    DashboardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AlertModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    HttpModule,
  ],
  providers: [
    HttpService,
    LoggedInGuard,
    AuthenticationService,
    StorageService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
