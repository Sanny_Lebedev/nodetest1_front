import { Component, OnInit } from '@angular/core';
import { StorageService } from './_services/storage.service';
import { AuthenticationService } from './_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './Templates/app.component.html',
  styleUrls: ['./css/app.component.scss']
})


export class AppComponent implements OnInit {

 constructor(
      private storageService: StorageService,
      private authenticationService: AuthenticationService,
      private router: Router,
    ) { }

  ngOnInit() {
    console.log(localStorage.getItem('currentRole'));
    // this.isAdmin = localStorage.getItem('currentRole') === 'Authorized';
    this.isAdmin = localStorage.getItem('currentRole') === 'Auth';
    this.storageService.watchStorage().subscribe(() => {
      this.isAdmin = localStorage.getItem('currentRole') === 'Auth';
      console.log('Changes detected');
     });
  }


  title = 'app';
  isCollapsed = true;

  isAdmin: boolean = localStorage.getItem('currentRole') === 'Authorized';
  changeValue(valid: boolean) {
    this.isAdmin = valid;
       }


       logout1 (){
       this.authenticationService.logout();
       this.router.navigate(['/login']);
       }

}
